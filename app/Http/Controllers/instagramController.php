<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Input;

class instagramController extends Controller
{

  public function viewInsta()
  {
    // header("Location: https://www.instagram.com/oauth/authorize/?client_id=c5a43acb45c14484bb5da2a48463a2e7&redirect_uri=http://localhost:8000/instagram&response_type=code");
    // $pageWasRefreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0';
    //header('Location: https://www.instagram.com/oauth/authorize/?client_id=c5a43acb45c14484bb5da2a48463a2e7&redirect_uri=http://localhost:8000/instagram&response_type=code');
    $client_id="c5a43acb45c14484bb5da2a48463a2e7";
    $client_secret="d45903d984904b9cbc9218ba10e792ad";
    $redirect_uri="http://localhost:8000/instagram";
    $code=Input::get('code');
    //access Token
    $fields = array(
              'client_id'     => $client_id,
              'client_secret' => $client_secret,
              'grant_type'    => 'authorization_code',
              'redirect_uri'  => $redirect_uri,
              'code'          => $code
       );
  //get Access Token
       $url = 'https://api.instagram.com/oauth/access_token';
       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($ch, CURLOPT_TIMEOUT, 20);
       curl_setopt($ch,CURLOPT_POST,true);
       curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
       $result = curl_exec($ch);
       curl_close($ch);
       $result = json_decode($result);
       $access_token=$result->access_token;

       // $access_token="15734693242.c5a43ac.5e0b84c07f134471a6252cfc5a98a942";

       $return = self::rudr_instagram_api_curl_connect("https://api.instagram.com/v1/users/self/media/recent/?access_token=".$access_token);
       return view("hrms.Instagram.viewinstagram", compact('return'));
  }


  public function rudr_instagram_api_curl_connect( $api_url )
  {
      $connection_c = curl_init(); // initializing
      curl_setopt( $connection_c, CURLOPT_URL, $api_url ); // API URL to connect
      curl_setopt( $connection_c, CURLOPT_RETURNTRANSFER, 1 ); // return the result, do not print
      curl_setopt( $connection_c, CURLOPT_TIMEOUT, 20 );
      $json_return = curl_exec( $connection_c ); // connect and get json data
      curl_close( $connection_c ); // close connection
      return json_decode( $json_return ); // decode and return
  }

  public function imageInsta() {
      // $username='weareone.exo';
      $username ='gregoryphil2';
      $insta_source = file_get_contents('https://www.instagram.com/'.$username.'/'); // instagram user url
      $shards = explode('window._sharedData = ', $insta_source);

      $insta_json = explode(';</script>', $shards[1]);
      $results_array = json_decode($insta_json[0], TRUE);

      $instaprofile_pic_hd=$results_array['entry_data']['ProfilePage'][0]['graphql']['user']['profile_pic_url_hd'];

      //echo $results_array;
      $limit = 100; // provide the limit thats important because one page only give some images.
      $image_array= array(); // array to store images.
      $image_url=array();
      $images=array();

      for ($i=0; $i < $limit; $i++) {
          //new code to get images from json
          if(isset($results_array['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'][$i])){
              $latest_array = $results_array['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'][$i]['node'];
              $image_data  = $latest_array['thumbnail_src']; // thumbnail and same sizes
              $image_code= "https://www.instagram.com/p/".$latest_array['shortcode']."/";
              //$image_data  = '<img src="'.$latest_array['display_src'].'">'; actual image and different sizes
              array_push($image_array, $image_data);
              array_push($image_url,$image_code);

          }

      }
      $images=array_combine($image_array,$image_url);


      return view("hrms.Instagram.imageInstagram", compact('image_array','instaprofile_pic_hd','username','images'));
          // foreach ($image_array as $image) {
          //     echo $image;// this will echo the images wrap it in div or ul li what ever html structure
          // }
       // this return a lot things print it and see what else you need
  }

}
?>
