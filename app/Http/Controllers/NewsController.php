<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Input;
class NewsController extends Controller
{
  public function latestNews()
  {
    // Australia
    $aus_news_source = file_get_contents('https://newsapi.org/v2/top-headlines?country=au&apiKey=09e79c815fd146b886bee017635aacc1');
    $shards = explode('window._sharedData = ', $aus_news_source);

    $aus_news_json = explode(';</script>', $shards[0]);
    $australia_news = json_decode($aus_news_json[0], TRUE);

    //Hong Kong
    $HK_news_source = file_get_contents('https://newsapi.org/v2/top-headlines?country=hk&apiKey=09e79c815fd146b886bee017635aacc1');
    $shards = explode('window._sharedData = ', $HK_news_source);

    $HK_news_json = explode(';</script>', $shards[0]);
    $hongKong_news = json_decode($HK_news_json[0], TRUE);

    //malaysia
    $MY_news_source = file_get_contents('https://newsapi.org/v2/top-headlines?country=my&apiKey=09e79c815fd146b886bee017635aacc1');
    $shards = explode('window._sharedData = ', $MY_news_source);

    $MY_news_json = explode(';</script>', $shards[0]);
    $malaysia_news = json_decode($MY_news_json[0], TRUE);

    return view("hrms.news.showNews",compact('australia_news','hongKong_news','malaysia_news'));
  }
}


?>
