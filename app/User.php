<?php

namespace App;
use App\Models\Employee;
use App\Models\Project;
use App\Models\UserRole;
use App\Models\Team;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function employee()
    {
        return $this->hasOne(Employee::class, 'user_id', 'id');
      //  return "STring";
    }

    public function role()
    {
        return $this->hasOne('App\Models\UserRole', 'user_id', 'id');
    }


    public function isHR()
    {
        $userId = Auth::user()->id;
        $userRole = UserRole::where('user_id', $userId)->first();
        if($userRole->role_id == 7 || $userRole->role_id == 1)
        {
            return true;
        }
        return false;
    }

    public function notAnalyst()
    {
        $userId = Auth::user()->id;
        $userRole = UserRole::where('user_id', $userId)->first();
        if($userRole->role_id != 3)
        {
            return true;
        }
        return false;
    }

    public function isCoordinator()
    {
        $userId = Auth::user()->id;
        $userRole = UserRole::where('user_id', $userId)->first();
        $roleId = [2,5,7,8,9,10,14,16];
        if(in_array($userRole->role_id, $roleId) )
        {
            return true;
        }
        return false;
    }

    public function isManager()
    {
        $userId = Auth::user()->id;
        $userRole = UserRole::where('user_id', $userId)->first();
        if($userRole->role_id == 16)
        {
            return true;
        }
        return false;
    }

    //Gonnie edit
    public function isTeam()
    {
      // $allTeams = DB::table(teams)->get();
      // $empLeaves = DB::table(employee_leaves)->get();
      // $userId = Auth::user()->employee->id;
      // $empTeam = Team::where('member_id',$userId);
      // if($empTeam->teamId=$userId)
      // {
      //   return true;
      // }
      // return false;
        $allTeams = Team::get();
        $allLeaves=EmployeeLeaves::get();

        foreach ($allTeams as $allTeam)
       {
         

         if($allTeam->member_id == \Auth::user()->employee->user_id)
         {
           return true;
           echo $allTeams;
         }
         if($allTeam->manager_id == \Auth::user()->employee->user_id)
         {
           return true;
         }

       }
       return false;
     }

    public function project()
    {
        return $this->hasMany(Project::class);
    }
}
