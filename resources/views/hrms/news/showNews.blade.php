@extends('hrms.layouts.base')
@section('content')
<!DOCTYPE html>
<html>
<meta http-equiv="refresh" content="60">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>
<h1 class="text-muted" style="text-align:center;">NEWS</h1>
  <br>
  <div class="w3-bar w3-border w3-light-grey">
    <button class="w3-bar-item w3-button" onclick="country('australia')">Australia</button>
    <button class="w3-bar-item w3-button" onclick="country('HK')">Hong Kong</button>
    <button class="w3-bar-item w3-button" onclick="country('malaysia')">Malaysia</button>
  </div>
<!-- ------------------ Australia ------------------ -->
<div id="australia" class="w3-container country" align="center" style="background-image: url('https://newsapi.org/images/flags/au.svg'); background-attachment: fixed;">
  <h2 style="text-align:left;  color:black; font-family:Brush Script MT, Brush Script Std, cursive;">AUSTRALIA</h2>
  <p style="text-align:right; color:black;">Refresh on:<script>document.write(new Date().getHours()+ ":" +new Date().getMinutes() + ":" +new Date().getSeconds())</script></p>
  @for($i=0;$i<=10;$i++)
  <div style="width:1004px; border: 2px solid grey; text-align:center; background-color:grey;">

    <p style="color:black; font-size:30px; font-family:Bradley Hand, cursive;">{{print_r(strtoupper($australia_news['articles'][$i]['title']),true)}}</p>
    <a href={{print_r($australia_news['articles'][$i]['url'],true)}}><img src={{print_r($australia_news['articles'][$i]['urlToImage'],true)}} style=" width:1000px; "/></a>
  </div>
    <br>
    <br>
  @endfor
</div>
<!-- ------------------ HK ------------------ -->

<div id="HK" class="w3-container country" align="center" style="display:none; background-image: url('https://newsapi.org/images/flags/hk.svg'); background-attachment: fixed;">
  <h2 style="text-align:left; color:black; font-family:Brush Script MT, Brush Script Std, cursive;">HONG KONG</h2>
  <p style="text-align:right; color:black;">Refresh on:<script>document.write(new Date().getHours()+ ":" +new Date().getMinutes() + ":" +new Date().getSeconds())</script></p>

  @for($i=0;$i<=10;$i++)
  <div style="width:1004px; border: 2px solid grey; text-align:center; background-color:grey;">

    <p style="color:black; font-size:30px; font-family:Bradley Hand, cursive; ">{{print_r($hongKong_news['articles'][$i]['title'],true)}}</p>
    <a href={{print_r($hongKong_news['articles'][$i]['url'],true)}}><img src={{print_r($hongKong_news['articles'][$i]['urlToImage'],true)}} style=" width:1000px; "/></a>
  </div>
    <br>
    <br>
  @endfor
</div>

<!-- ------------------ Malaysia ------------------ -->

<div id="malaysia" class="w3-container country" align="center" style="display:none; background-image: url('https://newsapi.org/images/flags/my.svg'); background-attachment: fixed;">
    <h2 style="text-align:left; color:black; font-family:Brush Script MT, Brush Script Std, cursive;">MALAYSIA</h2>
    <p style="text-align:right; color:black;">Refresh on:<script>document.write(new Date().getHours()+ ":" +new Date().getMinutes() + ":" +new Date().getSeconds())</script></p>
  @for($i=0;$i<=10;$i++)
  <div style="width:1004px; border: 2px solid grey; text-align:center; background-color:grey;">

    <p style="color:black; font-size:30px; font-family:Bradley Hand, cursive; ">{{print_r($malaysia_news['articles'][$i]['title'],true)}}</p>
    <a href={{print_r($malaysia_news['articles'][$i]['url'],true)}}><img src={{print_r($malaysia_news['articles'][$i]['urlToImage'],true)}} style=" width:1000px; "/></a>
  </div>
    <br>
    <br>
  @endfor
</div>

<script>
function country(country) {
  var i;
  var x = document.getElementsByClassName("country");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  document.getElementById(country).style.display = "block";
}
</script>

</body>
</html>

@endsection
