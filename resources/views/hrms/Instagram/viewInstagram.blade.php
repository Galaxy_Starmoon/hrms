@extends('hrms.layouts.base')

@section('content')
<div class="content">
    <section id="content" class="animated fadeIn">
    <h1 class="text-muted" style="text-align:center"> INSTAGRAM</h1>
    @foreach ($return->data as $post)
    <tr>
      <?php
        $profile_picture= $post->user->profile_picture;
        $profile_username=$post->user->username;
        $profile_link= "https://www.instagram.com/".$profile_username;
      ?>
    </tr>
    @endforeach
    <br>
    <br>
    <h3 class="text-muted" style="text-align:left">@ {{$profile_username}}</h3>
    <td class="text-center"><a href="{{$profile_link}}" class="fancybox"><img src="{{$profile_picture}}" height="250" width="250"/></a></td>
    <br>
    <br>
    <br>
    <br>
    <br>

    @foreach ($return->data as $post)
    <tr>
      <td class="text-center"><a href={{ $post->link}}  class="fancybox"><img src={{ $post->images->thumbnail->url}} height="500" width="500" /></a></td>
    </tr>
    @endforeach
    <h5 class="text-muted" style="text-align:left">© 2019 INSTAGRAM</h5>
</div>

@endsection
