@extends('hrms.layouts.base')

@section('content')
<div class="content">
    <section id="content" class="animated fadeIn">
    <h1 class="text-muted" style="text-align:center"> INSTAGRAM</h1>
    <br>
    <br>
    <a href='https://www.instagram.com/{{$username}}/' class="fancybox" style="text-align:left; color:grey;"><h3>@ {{$username}}</a></h3>
    <img src={{$instaprofile_pic_hd}} height="150" width="150" style="border-radius: 50%;"/>
    <br>
    <br>
    <br>
    <br>
    @foreach ($image_array as $image)
    <tr>
      <td class="text-center"><a href='{{$images[$image]}}' class="fancybox" ><img src={{$image}} height="250" width="250" style="border: 5px solid black;" /></a></td>
    </tr>
    @endforeach
    <br>
    <br>
    <h5 class="text-muted" style="text-align:left">© <script>document.write(new Date().getFullYear())</script> INSTAGRAM</h5>
</div>
@endsection
